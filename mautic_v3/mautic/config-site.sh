#!/bin/bash

# En cas de de duplication, recopie des données dans le volume

SOURCE=/tmp/sources
BASE_HTML=/var/www/html

if [ -d "$SOURCE" ]; then

   if [ ! -e "$BASE_HTML/.copied" ]; then
      echo "Copie des fichiers" >>/tmp/config-site_0.log
      rm -rf ${BASE_HTML:?}/* $BASE_HTML/.[!.]*
      cp -pRf $SOURCE/* $SOURCE/.[!.]* $BASE_HTML/
      rm -rf $SOURCE
      ln -snfT $BASE_HTML/ $BASE_HTML/"$VIRTUAL_HOST"
      touch $BASE_HTML/.copied
   fi
fi

if [ ! -e "$BASE_HTML/.base" ]; then
   echo "Installation d'origine : pas d'ancienne base URL"
   echo "Installation d'origine : pas d'ancienne base URL" >/tmp/config-site.log
   # Création du ou des nouveaux liens
   if [ -z "$CHEMIN" ]; then
      ln -snfT $BASE_HTML/ $BASE_HTML/"$VIRTUAL_HOST"
      echo "BASE=$VIRTUAL_HOST" >$BASE_HTML/.base
   else
      ln -snfT $BASE_HTML/ $BASE_HTML/"$CHEMIN"
      ln -snfT $BASE_HTML/ $BASE_HTML/"$VIRTUAL_HOST"
      echo "CHEMIN=$CHEMIN" >$BASE_HTML/.base
      echo "BASE=$VIRTUAL_HOST" >>$BASE_HTML/.base
   fi
else
   # Seul le chemin pourrait être différent
   echo "Il s'agit d'un redémarrage."
   echo "Il s'agit d'un redémarrage." >/tmp/config-site.log
   # Mise à jour de la base URL de Mautic
   ANCIENNE_URL=$(grep </var/www/html/app/config/local.php site_url | cut -d"'" -f4)
   echo "L'ancienne URL est : $ANCIENNE_URL" >>/tmp/config-site.log

   if [ -z "$CHEMIN" ]; then
      NOUVELLE_URL="https://$ECB_DOCKER_HOST:$NGINX_PORT/$VIRTUAL_HOST/"
      NOUVELLE_BASE="$VIRTUAL_HOST"
   else
      NOUVELLE_URL="https://$DOMAINE/$CHEMIN/$VIRTUAL_HOST/"
      NOUVELLE_BASE="$CHEMIN/$VIRTUAL_HOST"
   fi

   sed -i "s|$ANCIENNE_URL|$NOUVELLE_URL|g" /var/www/html/app/config/local.php
   echo "La nouvelle URL est $NOUVELLE_URL"
   echo "La nouvelle URL est $NOUVELLE_URL" >>/tmp/config-site.log

   # Suppression du ou des anciens liens
   ANCIEN_CHEMIN=$(grep <$BASE_HTML/.base ^CHEMIN | cut -d"=" -f2)
   ANCIENNE_BASE=$(grep <$BASE_HTML/.base ^BASE | cut -d"=" -f2)
   if [ -z "$ANCIEN_CHEMIN" ]; then
      rm $BASE_HTML/"$ANCIENNE_BASE"
   else
      rm $BASE_HTML/"$ANCIENNE_BASE"
      rm $BASE_HTML/"$ANCIEN_CHEMIN"
   fi
   echo "Suppression des anciens liens" >>/tmp/config-site.log

   # Création du ou des nouveaux liens
   if [ -z "$CHEMIN" ]; then
      ln -snfT $BASE_HTML/ $BASE_HTML/"$NOUVELLE_BASE"
   else
      ln -snfT $BASE_HTML/ $BASE_HTML/"$CHEMIN"
      ln -snfT $BASE_HTML/ $BASE_HTML/"$NOUVELLE_BASE"
   fi
   echo "Création des nouveaux liens" >>/tmp/config-site.log
fi
