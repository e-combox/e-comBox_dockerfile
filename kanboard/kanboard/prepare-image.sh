#!/bin/bash

# Copie des données du volume pour qu'elles soient intégrées à l'image

SOURCE=/tmp/sources
BASE_DATA=/var/www/app/data
BASE_PLUGINS=/var/www/app/plugins
BASE_CONF_NGINX=/etc/nginx

if [ -d "$SOURCE" ]; then
   rm -rf $SOURCE
fi

mkdir -p $SOURCE/data $SOURCE/plugins $SOURCE/conf
cp -pRf $BASE_DATA/* $BASE_DATA/.[!.]* $SOURCE/data
cp -pRf $BASE_PLUGINS/* $BASE_PLUGINS/.[!.]* $SOURCE/plugins
cp -pRf $BASE_CONF_NGINX/* $BASE_CONF_NGINX/.[!.]* $SOURCE/conf

# Pas de suppression du ou des liens car les sources ne sont pas dans un volume

if [ -e "$SOURCE/data/.copied" ]; then
   rm $SOURCE/data/.copied
fi

if [ -e "$SOURCE/plugins/.copied" ]; then
   rm $SOURCE/plugins/.copied
fi

if [ -e "$SOURCE/conf/.copied" ]; then
   rm $SOURCE/conf/.copied
fi
