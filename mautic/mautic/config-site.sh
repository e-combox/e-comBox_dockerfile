#!/bin/bash

# En cas de de duplication, recopie des données dans le volume

SOURCE=/tmp/sources
BASE_HTML=/var/www/html
NOUVELLE_BASE=$VIRTUAL_HOST

if [ -d "$SOURCE" ]; then

   if [ ! -e "$BASE_HTML/.copied" ]; then
      echo "Copie des fichiers" >>/tmp/config-site_0.log
      rm -rf ${BASE_HTML:?}/* $BASE_HTML/.[!.]*
      cp -pRf $SOURCE/* $SOURCE/.[!.]* $BASE_HTML/
      rm -rf $SOURCE
      ln -snfT $BASE_HTML/ $BASE_HTML/"$VIRTUAL_HOST"
      touch $BASE_HTML/.copied
   fi
fi

if [ ! -e "$BASE_HTML/.base" ]; then
   echo "Installation d'origine : pas d'ancienne base URL"
   echo "Installation d'origine : pas d'ancienne base URL" >>/tmp/config-site.log

   # Ajout de .base
   echo "NOUVELLE_BASE=$NOUVELLE_BASE" >$BASE_HTML/.base

   # Création du ou des nouveaux liens
   if [ -z "$CHEMIN" ]; then
      ln -snfT $BASE_HTML/ $BASE_HTML/"$VIRTUAL_HOST"

   else
      ln -snfT $BASE_HTML/ $BASE_HTML/"$CHEMIN"
      ln -snfT $BASE_HTML/ $BASE_HTML/"$VIRTUAL_HOST"
      # Ajout du chemin à .base
      echo "CHEMIN=$CHEMIN" >>$BASE_HTML/.base

   fi
else
   # Seul le chemin pourrait être différent
   echo "Il s'agit d'un redémarrage."
   echo "Il s'agit d'un redémarrage." >>/tmp/config-site.log
   # Mise à jour de la base URL de Mautic
   ANCIENNE_URL=$(cat </var/www/html/app/config/local.php | grep site_url | cut -d"'" -f4)
   echo "L'ancienne URL est : $ANCIENNE_URL" >>/tmp/config-site.log

   if [ -z "$CHEMIN" ]; then
      NOUVELLE_URL="https://$ECB_DOCKER_HOST:$NGINX_PORT/$VIRTUAL_HOST/"
   else
      NOUVELLE_URL="https://$DOMAINE/$CHEMIN/$VIRTUAL_HOST/"
   fi

   sed -i "s|$ANCIENNE_URL|$NOUVELLE_URL|g" /var/www/html/app/config/local.php
   echo "La nouvelle URL est $NOUVELLE_URL"
   echo "La nouvelle URL est $NOUVELLE_URL" >>/tmp/config-site.log

   # Suppression du ou des anciens liens
   ANCIEN_CHEMIN=$(cat $BASE_HTML/.base | grep ^CHEMIN | cut -d"=" -f2)
   ANCIENNE_BASE=$(cat $BASE_HTML/.base | grep ^BASE | cut -d"=" -f2)
   if [ -z "$ANCIEN_CHEMIN" ]; then
      rm $BASE_HTML/"$ANCIENNE_BASE"
   else
      rm $BASE_HTML/"$ANCIENNE_BASE"
      rm $BASE_HTML/"$ANCIEN_CHEMIN"
   fi
   echo "Suppression des anciens liens" >>/tmp/config-site.log

   # Création du ou des nouveaux liens
   if [ -z "$CHEMIN" ]; then
      ln -snfT $BASE_HTML/ $BASE_HTML/"$NOUVELLE_BASE"
   else
      ln -snfT $BASE_HTML/ $BASE_HTML/"$CHEMIN"
      ln -snfT $BASE_HTML/ $BASE_HTML/"$CHEMIN/$NOUVELLE_BASE"
   fi
   echo "Création des nouveaux liens" >>/tmp/config-site.log
fi

# Modification du fichier htaccess (https://github.com/mautic/mautic/issues/10913)
if [ -z "$CHEMIN" ]; then
   echo "La nouvelle base est $NOUVELLE_BASE" >>/tmp/config-site.log
   sed -i "s|<If \"%{REQUEST_URI} =~ m#^/.*|<If \"%{REQUEST_URI} =~ m#^/$NOUVELLE_BASE/(index\|index_dev\|upgrade/upgrade)\.php#\">|" "$BASE_HTML"/.htaccess >>/tmp/config-site.log 2>&1
else
   {
      echo "Le nouveau chemin est $CHEMIN"
      echo "La nouvelle base est $NOUVELLE_BASE"
   } >>/tmp/config-site.log

   sed -i "s|<If \"%{REQUEST_URI} =~ m#^/.*|<If \"%{REQUEST_URI} =~ m#^/$CHEMIN/$NOUVELLE_BASE/(index\|index_dev\|upgrade/upgrade)\.php#\">|" "$BASE_HTML"/.htaccess >>/tmp/config-site.log 2>&1
fi
echo "Mise à jour du fichier .htacess" >>/tmp/config-site.log
