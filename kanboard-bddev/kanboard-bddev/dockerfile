FROM kanboard/kanboard:v1.2.21

# Ajout des applications utiles
RUN apk update \
 && apk add --no-cache less vim tzdata mariadb-client \
 && ln -s /usr/share/zoneinfo/Europe/Paris /etc/localtime \
 && apk del tzdata 

ADD conf-nginx.tar.gz /etc/nginx/
ADD plugins.tar.gz /var/www/app/plugins/
ADD data.tar.gz /var/www/app/data/

RUN openssl req -new -subj "/C=FR/ST=France/L=Corse/O=Reseaucerta/CN=localhost" -newkey rsa:4096 -x509 -sha256 -days 365 -nodes -out /etc/nginx/ssl/kanboard.crt -keyout /etc/nginx/ssl/kanboard.key \
 && chmod 400 /etc/nginx/ssl/kanboard.key

RUN chown -R nginx.nginx /var/www/app/data/*
RUN chown -R nginx.nginx /var/www/app/plugins/*

# Ajout du script de préparation des images en vue d'une éventuelle duplication
COPY prepare-image.sh /tmp/
RUN chmod +x /tmp/prepare-image.sh

# Ajout d'un script qui change notamment les URL
COPY config-site.sh /tmp/
RUN chmod +x /tmp/config-site.sh

# Exécution du script via entrypoint
RUN sed -i '$i\bash /tmp/config-site.sh\n' /usr/local/bin/entrypoint.sh
