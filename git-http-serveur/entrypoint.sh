#!/bin/sh
set -e

# Démarrage de cron
/usr/sbin/crond  &

# Démarrage de nginx
spawn-fcgi -s /run/fcgiwrap.sock -u nginx -U nginx -g nginx -- /usr/bin/fcgiwrap
nginx -g "daemon off;"

# wait until all processes end (wait returns 0 retcode)
while :; do
    if wait; then
        break
    fi
done
