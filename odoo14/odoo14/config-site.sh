#!/bin/bash

SOURCE=/tmp/sources
BASE_HTML=/var/lib/odoo
BASE_ADDONS=/mnt/extra-addons
BASE_CONFIG=/etc/odoo

if [ -d "$SOURCE" ]; then

  if [ ! -e "$BASE_HTML/.copied" ]; then
    echo "Copie des fichiers HTML" >>/tmp/config-site_1.log
    rm -rf ${BASE_HTML:?}/* $BASE_HTML/.[!.]*
    cp -pRf $SOURCE/html/* $SOURCE/html/.[!.]* $BASE_HTML/
    rm -rf $SOURCE/html
    touch $BASE_HTML/.copied
  fi

  if [ ! -e "$BASE_ADDONS/.copied" ]; then
    echo "Copie des modules" >>/tmp/config-site_1.log
    rm -rf ${BASE_ADDONS:?}/* $BASE_ADDONS/.[!.]*
    cp -pRf $SOURCE/addons/* $SOURCE/addons/.[!.]* $BASE_ADDONS/
    rm -rf $SOURCE/addons
    touch $BASE_ADDONS/.copied
  fi

  if [ ! -e "$BASE_CONFIG/.copied" ]; then
    echo "Copie des fichiers HTML" >>/tmp/config-site_1.log
    rm -rf ${BASE_CONFIG:?}/* $BASE_CONFIG/.[!.]*
    cp -pRf $SOURCE/config/* $SOURCE/config/.[!.]* $BASE_CONFIG/
    rm -rf $SOURCE/config
    touch $BASE_CONFIG/.copied
  fi
  rm -rf $SOURCE
fi

# Mise à jour d'odoo.conf avec le nouveau mot de passe d'accès à la base de données
sed -i "s/db_password.*/db_password = $DB_PASS/g" /etc/odoo/odoo.conf
